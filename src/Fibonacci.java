import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Fibonacci {
    private static Scanner console = new Scanner(System.in);
    private  static Map<Long , BigInteger> map = new HashMap<>();

    private static BigInteger countFib(long n){
        BigInteger sum;
        if(map.containsKey(n)){
            sum = map.get(n);
        }else if(BigInteger.valueOf(n).equals(BigInteger.ONE) || BigInteger.valueOf(n).equals((BigInteger.valueOf(2L)))){
            sum = BigInteger.ONE;
            map.put(n, BigInteger.ONE);
        } else{
            sum = countFib(n - 1).add(countFib(n - 2));
            map.put(n , sum);
        }
        return sum;
    }

    public static void main(String[] args) {
        long input = console.nextLong();
        while (input != 0){
            long before = System.currentTimeMillis();
            System.out.println(String.format("Fib: %d",countFib(input)));
            long after = System.currentTimeMillis();
            System.out.println(String.format("Millis left: %d",after-before));
            input = console.nextLong();
        }
    }
}
